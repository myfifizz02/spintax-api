<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use DB;

class Account extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_rtt',function( Blueprint $table ){
            $table->bigIncrements('id');
            $table->string('email');
            $table->string('nick');
            $table->string('key');
            $table->string('role');
            $table->string('status');
            $table->string('type');
            $table->dateTime('exp_acc');
            $table->string('reg_site');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('akun');
    }
}
