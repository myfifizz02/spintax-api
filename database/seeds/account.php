<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class account extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = env('EMAIL_DEV');
        $nick = env('NICK_DEV');
        $n_key = base64_encode( $email." ".$nick );
        DB::table('account_rtt')->insert([
            'email' => $email,
            'nick'  =>  $nick,
            'key'   => $n_key,
            'role'  => 'admin',
            'status'=>  'lifetime',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
    }
}
