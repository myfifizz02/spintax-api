<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('check/index','Checkcontroller@index');
$router->post('reg_pl','Checkcontroller@checker');
$router->post("check/site","CheckController@check_site");
$router->post('ac/status','AccountController@status');
$router->post("check/purchase","CheckController@check_purchase");
$router->post('spintext',"CheckController@spintext");
$router->post('spin_text','CheckController@spin_text');
$router->get('inst','CheckController@ins_to');
$router->get('get_syn','CheckController@get_synonims');
$router->get('test/mongo','CheckController@index');
$router->group(['middleware' => 'auth'],function () use ($router){
   $router->get('auth/test','Checkcontroller@index'); 
   $router->post('ac/create','AccountController@crt');
   $router->post("ac/renew","AccountController@renew");
});
