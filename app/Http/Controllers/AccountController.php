<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account_Rtt;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    function crt(Request $req){
        $data = $req->all();
        //create new account
        $que = Account_Rtt::where('id',$data['id'])->first();
        if( $que->role == 'admin'){
            $check = Account_Rtt::where('email',$data['email'])->first();
            if ( !is_null($check) ) {
                return response("Email telah digunakan gunakan email yang lain",409);
            }
            $ac =   new Account_Rtt;
            $ac->email = $data['email'];
            $ac->nick = $data['nick'];
            $n_key = base64_encode($data['email']." ".$data['nick']);
            $ac->key = $n_key;
            $ac->role = 'user';
            $ac->save();

            return response('user success registered',200);
        }
    }


    function status(Request $req){
        $data = $req->all();
        $site ;
        $status = 'limited';
        $nowadays = Carbon::now()->setTimezone('Asia/Jakarta')->timestamp;
        if(!Cache::has($data['key'])){
            if( is_null($data_purchase) ){
                return response('data not found',404);
            }else{
            $data_purchase = DB::connection('mysql2')->table('rtk_purchases')->where('PC_KEY',$data['key'])->where('PC_BUYER_U_ID',$data['email'])->first();
            $usr_meta = DB::connection("mysql2")->table('rtk_usermeta')->where('U_ID',$data['email'])->where('meta_key','site_autorotate_reg')->first();
            if( is_null($usr_meta) ) return response('User meta not found',404);
            $exp_accs = Carbon::parse($data_purchase->PC_EXPIRED,"+07:00");
            if( $nowadays > $exp_acc->timestamp ){
                $status = 'expired';
            }
            $arr_data = [
                'user_id'   =>  $data_purchase->PC_BUYER_U_ID,
                'exp_acc'   =>  $data_purchase->PC_EXPIRED,
                'site'  =>  $usr_meta->meta_value,
                'status'    =>  $status
            ];
            Cache::store('redis')->put($data['key'],$arr_data);
            return $status;
          }  
        }else{
            $cache_data = Cache::get($data['key']);
            $exp_acc = Carbon::parse($cache_data['exp_acc'],'+07:00');
            if( $nowadays > $exp_acc->timestamp ) {
                $cache_data['status'] = "expired";
                Cache::store('redis')->put($data['key'],$cache_data);
                return "expired";
            }else{
                return $cache_data['status'];
            }
        } 
    }

    function renew(Request $req){
        $data = $req->all();
        if( !Cache::has($data['key']) ) return response('Data Account not found',404);
        $qry = Cache::get($data['key']);
        if($qry['status'] == 'expired'){
            $qry['status'] == 'limited';
        }
        $qry['exp_acc'] == $data['exp_date'];
        Cache::set($data['key'],$qry);

        return response("success",200);
    }

}