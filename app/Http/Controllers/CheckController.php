<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use App\Mongod;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Mongod2;

class CheckController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    function index(){
       //$conn = new Mongod2;
       dd(Mongod2::where('word','karena')->first());
    }

    function checker(Request $req){
        $data = $req->all();
        //check purchase key on store
       $some_data = DB::connection("mysql2")->table('rtk_purchases')->where('PC_KEY',$data['purc_k'])->where('PC_BUYER_U_ID',$data['email'])->first();
       //another checker 
       if(is_null($some_data)) return response('data tidak ada',404);
       $nowadays = Carbon::now()->setTimezone('Asia/Jakarta')->timestamp;
       $purc_exp = Carbon::parse($some_data->PC_EXPIRED,'+07:00');
       if($nowadays > $purc_exp->timestamp) return response("Your account was expired",406);

        //checking usermeta
        $usr_meta = DB::connection("mysql2")->table('rtk_usermeta')->where('U_ID',$data['email'])->where('meta_key','site_autorotate_reg')->first();
        if ( is_null($usr_meta) ) {
            DB::connection("mysql2")->table('rtk_usermeta')->insert([
                'U_ID'  =>  $data['email'],
                'meta_key'  =>  'site_autorotate_reg',
                'meta_value'    =>  $data['site'],
            ]);
        }else{
            DB::connection("mysql2")->table('rtk_usermeta')->where('U_ID',$data['email'])->where('site_autorotate_reg')->update([
                'meta_value'    =>  $data['site']
            ]);
        }

       // checking cache , if null create new cache
        if(Cache::has($data['purc_k'])){
            $data_cache = Cache::get($data['purc_k']);
            $exp_pc = Carbon::parse($data_cache['exp_acc'],'+07:00');
            if( $nowadays > $exp_pc->timestamp ){
                return response("This account was expired",406);
            }
            
            if( !empty($data_cache['site']) ){
                if($data_cache['site'] == $data['site'] ) return json_encode($data_cache);
                return response('Purchase is used another site');
            }
            $data_cache['site'] = $data['site'];
            Cache::set($data['key'],$data_cache);
            return json_encode($data_cache);
        } else{

            //get some data for caching
            $arr_data = [
                'user_id' => $data['email'],
                'exp_acc' => $some_data->PC_EXPIRED,
                'site'  =>  $data['site'],
                'status'    =>  'limited',
            ];
            Cache::store('redis')->put($data['purc_k'],$arr_data);
            return json_encode($arr_data);
        }

    }


    function check_purchase(Request $req){
        $data = $req->all();
        //"SELECT * FROM rtk_purchases WHERE SALES_BUYER_U_ID = $data['email'] WHERE PC_KEY = $data['pur_key'] "
        $src = DB::connection("mysql2")->table('rtk_purchases')->where('SALES_BUYER_U_ID',$data['email'])->where('PC_KEY',$data['key'])->first();
        if( is_null($src) ) return response("data tidak ada",404);
        if( Cache::has($data['key']) ){
            $data_check = Cache::get($data['key']);
            if($data_check['status'] == 'expired') return response('Your Account still expired',406);
            $nows =  Carbon::now()->setTimezone('Asia/Jakarta')->timestamp;  
            $exp_date = Carbon::parse($data_check['exp_acc'],"+07:00")->timestamp;
            if( $exp_date < $nows ) return response("reactivated was failed , access was expired",406) ;
            return json_encode($data_check['site']);
        }
    }


    function check_site(Request $req){
        $data = $req->all();

        if (!Cache::has($data['key'])) return response("data not found !",404);
        
        $data_cache = Cache::get($data['key']);
        if( empty($data_cache['site']) ) return response("Your account not register any site",404);
        $arr_site = explode(",",$data_cache['site']);
        if ( !in_array($data['site'],$arr_site) ) return response("your site are not registered",404);

        if( $data_cache['status'] == 'expired' ) return response('Account is expired',401);
        
    }

    function spintext(Request $req){
        $data = $req->all();
        $arr_orig = explode(" ",$data['orig_word']);
        $low_exc = strtolower($data['exclude_word']);
        $arr_exc = explode(",",$low_exc);
        $arrs = [];
        foreach($arr_orig as $value){
            $lower_val = strtolower($value);
            $cari = Mongod2::where('word',$lower_val)->first();
            $unlist_synonim = Cache::store('file')->get('unlist_synonim');
            if(in_array($lower_val,$arr_exc)){
                $arrs[] = $value;
            }else if( !empty($unlist_synonim) && in_array($value,$unlist_synonim)){
                $arrs[] = $value;
            }else{

                if (!is_null($cari)) {
                    $arr_temps = $cari['synonim'];
                    shuffle($arr_temps);
                    $arrs_temp2 = array_splice($arr_temps,0,5);
                    $f_word = "{".implode("|",$arrs_temp2)."}";
                    $arrs[] = $f_word;
                }else{
                    // scrap data 
                    $client =  new Client;
                    $rest = $client->request('POST','https://www.persamaankata.com/search.php',[
                        'form_params' => [
                            'q' =>  $value
                        ]
                    ]);
                    $regs = '/<div class="word_thesaurus">(.*?)<\/div>/s';
                    $temp = [];
                    $f_temp = [];
                    if ( preg_match($regs, $rest->getBody(), $list) ){
                        $rest_word = strip_tags($list[1]);
                        $rest_arr = explode(",",$rest_word);

                        $arrs_temp = $rest_arr;
                        shuffle($arrs_temp);
                        $arrs_temp2 = array_splice($arrs_temp,0,5);   
                        $f_temp = "{".implode("|",$arrs_temp2)."}";
                        $arrs[] = $f_temp;    
                        $mongo = new Mongod2;
                        $mongo->word = $value;
                        $mongo->synonim = $rest_arr;
                        $mongo->save();

                    }else{
                        $arrs[] = $value;
                        //save to cache
                        $unlist_synonim = Cache::store('file')->get('unlist_synonim');
                        $unlist_synonim[] = $value;
                        Cache::store('file')->put('unlist_synonim',$unlist_synonim);
                    } 
                                    
                }
            }
        }
        $f_arr = implode(" ",$arrs);
        return $f_arr;
    }

    function ins_to(){
        $file_loc = realpath( __DIR__.'/../../../resources/results.json' );
        $file_json = json_decode(file_get_contents($file_loc),true);
        // $conn =  new Neo4j;
        // $client = $conn->clients();
        foreach ($file_json as $keys ) {
        //    foreach ($keys as $key => $value) {
        //        //doing with key
        //        $client->run('create (a:words{value:"'.$key.'"})');
        //        foreach ($value as $values ) {
        //            $client->run(' 
        //            match (n:words{value:"'.$key.'"})
        //            create (b:words{value:"'.$values.'"}) 
        //            create (b)-[:SINONIM]->(n)');
        //        }
        //    }
        }
    }

    function test_mongo(){
        $query = Mongod::all();
        foreach ($query as $key ) {
            
        }
    }

    function process_spin($text){
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*?)\}/x',
            array($this, 'replace_spintext'),
            $text
        );
    }

    function replace_spintext($text){
        $text = $this->process_spin($text[1]);
        $parts = explode('|',$text);
        return $parts[array_rand($parts)];
    }
    function spin_text(Request $req){
        $data = $req->all();
        $checker = new CheckController;
        $result_spintext = $checker->process_spin($data['result_spin']);
        Log::info($result_spintext);
        return $result_spintext;
    }

    function get_synonims(){
       $client =  new Client;

       $query = Mongod::all();
       foreach($query as $value){
            $result = $client->request('POST','https://www.persamaankata.com/search.php',[
                'form_params' => [
                    'q' =>  $value->word
                ]
            ]);
            $regs = '/<div class="word_thesaurus">(.*?)<\/div>/s';
            if( preg_match($regs,$result->getBody(),$list) ){
                    $words = strip_tags($list[1]);
                    //result
                    $words_arr = explode(',',$words);
                    $querys = new Mongod2;
                    $querys->word = $value->word;
                    $querys->synonim = $words_arr;
                    $querys->save();
                    
            }
       }
       echo 'done insert';
    }
    
}
