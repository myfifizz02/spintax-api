<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Account_Rtt;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            if( $request->has('id') ){
                $data = $request->input('data');
                $ac = Account_Rtt::where('id', $request->input('id'))->first();
                if( !hash_equals(hash_hmac('sha256',$data,$ac->key),$request->input('hash')) ){
                    
                    return response('Akses Ditolak, Permission not allowed !', 401);
                }
            }else{
                return response('Akses ditolak, Who Are you ?.', 401);
            }
        }
        $dec_data = json_decode($request->input('data'),true);
        if ( !is_null($dec_data) ) 
            $request->merge($dec_data);
        return $next($request);
        
    }
}
