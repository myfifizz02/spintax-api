<?php
namespace app;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * for modified data on mongodb
 */
class Mongod extends Eloquent 
{
    protected $connection = 'mongodb';
    protected $collection  = 'list_synonims';
}