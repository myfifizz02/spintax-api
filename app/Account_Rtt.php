<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class Account_Rtt extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    protected $table = 'account_rtt';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','nick','key','role','status','exp_acc','reg_site'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    /** 
     * checking expired account
    */
     public static function check_exp($id){
         $query = Account_Rtt::find($id);
         if( is_null($query) ){
             return 'not found'; 
        } 
         if($query->status == 'expired') {
             return 'expired';
            }
         $now_time = Carbon::now()->setTimezone('Asia/Jakarta')->timestamp;
         $exp_date = Carbon::parse($query->exp_acc,'+07:00');
         if( $exp_date->timestamp < $now_time) {
             $query->status = 'expired';
             $query->save();
         }
     }
}
