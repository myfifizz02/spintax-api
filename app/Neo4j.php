<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use GraphAware\Neo4j\Client\ClientBuilder;
use Illuminate\Support\Facades\Log;
/**
 * for modified data with neo4j graphaware
 */
class Neo4j extends Model {

   public function __construct()
   {
       
   }

   function clients(){
        $usr = env('USER_NEO4J');
       $pass = env('PASSW_NEO4J');
       $url_def = "http://".$usr.":".$pass."@localhost:7474";
       $url_bolt = "bolt://".$usr.":".$pass."@localhost:7687";
       $client = ClientBuilder::create()
       ->addConnection('default',$url_def)
       ->addConnection('bolt',$url_bolt)
       ->setDefaultTimeout(300)
       ->build();
      return $client;
   }

   /**
    * insert data to neo4j
    *
    */
    public static function inst($vals){
      $conn  = new Neo4j;
      $client = $conn->clients();
    $client->run('CREATE (n:words) SET n += {infos}',['infos' => $vals]);
    }

    public function cari($qry){
       $conn = new Neo4j;
       $client = $conn->clients;
       $rest = $client->run($qry);
       Log::info(print_r($rest,true));
       return $this;
       if(!empty($rest->getRecords())){
          foreach ($rest->getRecords() as $key ) {
          }
       }
    }
}